/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.controller;

/** 
 * DTO for Table object. Used to fill up variables for table. Required because table requires objects from both 
 * SupportDTO and SupportReplyDTO. Also can contain custom settings used by the table.
 * @author Jonathan
 */
public class ITSupportTableRecord {
    private int tableId;            //ID of the contents of the table.
    private String tableTitle;      //Title of the record on teh table.
    private String tableContent;    //Content of the record on table.
    private String sentTime;        //Sent time of the object on the table.
    private String status;          //Status of teh record
    private int sentUserId;         //ID of the sent user.
    private int origMessageId;      //ID of the original message. THe first message sent by the user.
    private String style;           //Cotnrols stylings of the row.

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getTableTitle() {
        return tableTitle;
    }

    public void setTableTitle(String tableTitle) {
        this.tableTitle = tableTitle;
    }

    public String getTableContent() {
        return tableContent;
    }

    public void setTableContent(String tableContent) {
        this.tableContent = tableContent;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSentUserId() {
        return sentUserId;
    }

    public void setSentUserId(int sentUserId) {
        this.sentUserId = sentUserId;
    }

    public int getOrigMessageId() {
        return origMessageId;
    }

    public void setOrigMessageId(int origMessageId) {
        this.origMessageId = origMessageId;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
