/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.controller;

import au.com.enterprises.lev.modified.Access.ITSupportReplyDAO;
import au.com.enterprises.lev.modified.model.ITSupportReplyDTO;
import au.com.enterprises.lev.modified.staticClasses.User;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;

/**
 * Controller for the IT Reply objects.
 *
 * @author user
 */
@Named
@SessionScoped
public class ITSupportReplyController implements Serializable {

    private final ITSupportReplyDAO mReplyDAO = new ITSupportReplyDAO();  //setup access object
    private ITSupportReplyDTO itReply = new ITSupportReplyDTO();    //setup transfer object
    private int iTSupportId;                                        //ITSupport ID.

    /**
     * Returns all replies found from the DAO.
     *
     * @param suppId - Support ID of all replies to be returned.
     * @return
     * @throws SQLException
     * @throws NamingException
     * @throws NoSuchAlgorithmException
     */
    public ArrayList<ITSupportReplyDTO> getReplies(int suppId) throws SQLException, NamingException, NoSuchAlgorithmException {
        mReplyDAO.startConnection();
        ArrayList<ITSupportReplyDTO> replies = mReplyDAO.findReplies(suppId);
        mReplyDAO.endConnection();
        return replies;
    }

    /**
     * Add a reply request made by a user.
     *
     * @return
     */
    public String addUserReply() {
        //set up some variables not in the html form.
        itReply.setSentTime(new Date());
        itReply.setItSuppId(iTSupportId);
        itReply.setUserId(User.getUserId());
        itReply.setITReply(false);
        itReply.setUserRead(true);
        new ITSupportController().isRead(iTSupportId, true, false);
        //write to table.
        try {
            mReplyDAO.startConnection();
            mReplyDAO.writeToTable(itReply);
            mReplyDAO.endConnection();
        } catch (SQLException | NamingException | NoSuchAlgorithmException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return ("ticketList");
    }

    /**
     * Add a reply to user by the IT.
     *
     * @return
     */
    public String addITReply() {
        //setup variables not in HTML form.
        itReply.setSentTime(new Date());
        itReply.setItSuppId(iTSupportId);
        itReply.setUserId(User.getUserId());
        itReply.setITReply(true);
        itReply.setUserRead(false);
        new ITSupportController().isRead(iTSupportId, true, true);
        try {
            //write to table.
            mReplyDAO.startConnection();
            mReplyDAO.writeToTable(itReply);
            mReplyDAO.endConnection();
        } catch (SQLException | NamingException | NoSuchAlgorithmException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        new ITSupportController().editStatus("Pending", itReply.getItSuppId());
        return ("ticketList");
    }

    public void isRead(int id) {
        ITSupportReplyDTO ITSupport = findObjectFromId(id);
        ITSupport.setUserRead(true);
        try {
            mReplyDAO.startConnection();
            mReplyDAO.editRecord(ITSupport);
            mReplyDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    private ITSupportReplyDTO findObjectFromId(int id) {
        ITSupportReplyDTO ITSupport = new ITSupportReplyDTO();
        try {
            mReplyDAO.startConnection();
            ITSupport = mReplyDAO.findByReplyID(id);
            mReplyDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return (ITSupport);
    }
    
    public ITSupportReplyDTO getItReply() {
        return itReply;
    }

    public void setItReply(ITSupportReplyDTO itReply) {
        this.itReply = itReply;
    }

    public String setITSupportId(int iTSupportId) {
        this.iTSupportId = iTSupportId;
        return "/faces/allusers/ITSupport/replyMessage.xhtml";
    }
}
