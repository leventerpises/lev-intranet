/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.controller;

import au.com.enterprises.lev.modified.Access.AccountDAO;
import au.com.enterprises.lev.modified.Access.MessageBusDAO;
import au.com.enterprises.lev.modified.model.MessageBusDTO;
import au.com.enterprises.lev.modified.staticClasses.Time;
import au.com.enterprises.lev.modified.staticClasses.User;
import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 * Connects interface to DAO
 *
 * @author Anson
 */
@Named
@SessionScoped
public class MessageBusController implements Serializable{

    //A message object to store the selected message in a table
    private MessageBusDTO selectedMessage;
    //A message object to store a message object to be written into the database
    private MessageBusDTO writeMessage;
    //A string to be displayed onto a text box when a message is selected
    private String fullMessage;
    //The Data Access Object for the controller
    private MessageBusDAO mMessageAccess;
    //An array list of messages to be displayed in a table
    private ArrayList<MessageBusDTO> messages;
    //Determines if user is requesting for received or sent messages
    private boolean showReceivedMessages;

    /**
     * initializes all variables
     */
    @PostConstruct
    public void onConstruct() {
        writeMessage = new MessageBusDTO();
        selectedMessage = null;
        fullMessage = new String();
        mMessageAccess = new MessageBusDAO();
        showReceivedMessages = true;
    }

    /**
     * Adds Message into Database using the DAO
     * @param userId
     * @return
     */
    public String addMessage(int userId){
        try {
            mMessageAccess.startUp();
            writeMessage.setSenderId(userId);
            mMessageAccess.add(writeMessage, new Timestamp(new Date().getTime()));
            mMessageAccess.endConnection();
        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(ex.getMessage()));
        }            
        return "/faces/allusers/Message/table.xhtml";
    }

    /**
     * Retrieves all received message by the user
     * @throws SQLException
     * @throws NamingException 
     */
    private void getReceiverMessages(){
        try {
            mMessageAccess.startUp();
            messages = mMessageAccess.findByReceiver(User.getUserId());
            mMessageAccess.endConnection();
        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(ex.getMessage()));
        }
    }

    /**
     * Retrieves all sent message by the user 
     */
    public void getSenderMessages(){
        try {
            mMessageAccess.startUp();
            messages = mMessageAccess.findBySender(User.getUserId());
            mMessageAccess.endConnection();
        } catch (SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(ex.getMessage()));
        }
    }

    /**
     * This function is called when the user selects a row from a table
     * @param event
     */
    public void onRowSelect(SelectEvent event){
        try {
            selectedMessage = (MessageBusDTO) event.getObject();
            if (selectedMessage.isRead() == false) {
                Timestamp time = new Timestamp(new Date().getTime());
                selectedMessage.setReadTime(Time.getShortDate(time));
                mMessageAccess.startUp();
                mMessageAccess.update(selectedMessage, time);
                mMessageAccess.endConnection();
            }
            fullMessage = selectedMessage.getMessageTitle()
                    + "\nSender : " + new AccountDAO().getNameFromID(selectedMessage.getSenderId())
                    + "     Sent Time: " + selectedMessage.getSentTime()
                    + "\nMessage: " + selectedMessage.getMessageContent();
        } catch (NamingException | SQLException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(ex.getMessage()));
        }
    }

    /**
     * This function is called when the user deselects a row from a table
     * @param event 
     */
    public void onRowUnselect(UnselectEvent event) {
        selectedMessage = null;
        fullMessage = "";
    }

    public MessageBusDTO getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(MessageBusDTO selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public MessageBusDTO getWriteMessage() {
        return writeMessage;
    }

    public void setWriteMessage(MessageBusDTO writeMessage) {
        this.writeMessage = writeMessage;
    }

    public String getFullMessage() {
        return fullMessage;
    }

    public void setFullMessage(String fullMessage) {
        this.fullMessage = fullMessage;
    }

    public ArrayList<MessageBusDTO> getMessages() throws SQLException, NamingException {
        if (showReceivedMessages) {
            getReceiverMessages();
        } else {
            getSenderMessages();
        }
        return messages;
    }

    public void setMessages(ArrayList<MessageBusDTO> messages) {
        this.messages = messages;
    }

    /**
     * Returns whether the format is bold or not
     * @param isRead
     * @return 
     */
    public String style(boolean isRead) {
        String style = "";
        if (showReceivedMessages) {
            style = isRead ? "" : "font-weight:bold";
        }
        return style;
    }

    public boolean isShowReceivedMessages() {
        return showReceivedMessages;
    }

    public void setShowReceivedMessages(boolean showReceivedMessages) {
        this.showReceivedMessages = showReceivedMessages;
    }

}
