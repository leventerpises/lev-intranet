/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.controller;

import au.com.enterprises.lev.modified.Access.AccountDAO;
import au.com.enterprises.lev.modified.model.*;
import au.com.enterprises.lev.modified.staticClasses.*;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 * Controller for the IT Support table. Required because the table requires both
 * ITReply and ITSupport objects. Also adds fields specific to the table such as
 * style of the text to send to the table.
 *
 * @author Jonathan
 */
@Named
@SessionScoped
public class ITSupportTableController implements Serializable {

    private ITSupportTableRecord selectedMessage = new ITSupportTableRecord();      //message selected by the user on the table.
    private String iTSupportMessage = new String();             //object used to output the correct message. displayed to the user.
    private boolean replyDisabledState = true;
    private boolean showActiveTickets = true;
    private String currentSelectedStatus;

    /**
     * Object used to get the records for the IT table. Retrieves all records in
     * table.
     *
     * @return
     */
    public ArrayList<ITSupportTableRecord> fetchITTable() {
        ITSupportController supportCont = new ITSupportController();
        ArrayList<ITSupportDTO> itMessages;
        if (showActiveTickets) {
            itMessages = supportCont.getActiveMessages();
        } else {
            itMessages = supportCont.getCompletedMessages();
        }
        return setUpTable(itMessages, true);
    }

    /**
     * Fetch table for the user. Gets records based on the current user calling
     * the class.
     *
     * @return
     */
    public ArrayList<ITSupportTableRecord> fetchUserTable() {
        ITSupportController supportCont = new ITSupportController();
        ArrayList<ITSupportDTO> itMessages;
        if (showActiveTickets) {
            itMessages = supportCont.getUserActiveMessages();
        } else {
            itMessages = supportCont.getUserCompletedMessages();
        }
        return setUpTable(itMessages, false);
    }

    /**
     * Class called by the two fetch table requests above. changes ITSupport
     * class and Reply objects to Table object and gets the table ready to be
     * displayed in the HTML.
     *
     * @param itMessages - Array of ITSupport object to be converted to ITTable
     * object.
     * @param isIT - result of a failed attempt to make unread message bold will
     * be changed and deleted later.
     * @return
     */
    private ArrayList<ITSupportTableRecord> setUpTable(ArrayList<ITSupportDTO> itMessages, boolean isIT) {
        ArrayList<ITSupportTableRecord> messages = new ArrayList<>();
        ITSupportTableRecord tableMessage;
        for (ITSupportDTO messageTmp : itMessages) {
            //convert ITSupport object to ITTable object.
            tableMessage = new ITSupportTableRecord();
            tableMessage.setTableId(messageTmp.getItSuppId());
            tableMessage.setTableTitle(messageTmp.getRequestTitle());
            tableMessage.setTableContent(messageTmp.getRequestContent());
            tableMessage.setSentTime(Time.getShortDate(messageTmp.getSentTime()));
            tableMessage.setSentUserId(messageTmp.getUserId());
            tableMessage.setOrigMessageId(messageTmp.getItSuppId());
            tableMessage.setStatus(messageTmp.getStatus());
            tableMessage.setStyle("");
            if ((isIT && !messageTmp.isItRead()) || (!isIT && !messageTmp.isUserRead())) {
                tableMessage.setStyle(tableMessage.getStyle() + "font-weight:bold;");
            }
            messages.add(tableMessage);
            //IF the user has selected a message, display the replies of that message.
            if (selectedMessage.getOrigMessageId() == messageTmp.getItSuppId()) {
                messages = setReplies(messages, messageTmp.getItSuppId(), isIT);
            }

        }
        return messages;
    }

    /**
     * Takes in an ArrayList of ITTableDTO objects, modifies it and returns
     * modified table. Function adds all replies of the selected object.
     *
     * @param itMessages - ArrayList of ITTableDTO to be modified by this
     * function.
     * @param suppId - ID of the selected IT Support object.
     * @param isIT - Result of a failed attempt to make unread messages bold,
     * will be removed and imp[roved upon later.
     * @return
     */
    private ArrayList<ITSupportTableRecord> setReplies(ArrayList<ITSupportTableRecord> itMessages, int suppId, boolean isIT) {
        ArrayList<ITSupportTableRecord> messages = itMessages;
        ITSupportReplyController replyCont = new ITSupportReplyController();
        ArrayList<ITSupportReplyDTO> supportMessages = new ArrayList<>();
        ITSupportTableRecord tableMessage;
        try {
            //Checks if selected IT Support message has replies. if it does returns the replies.
            supportMessages = replyCont.getReplies(suppId);
        } catch (SQLException | NamingException | NoSuchAlgorithmException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }

        //convert found replies to ITSupport messages.
        boolean makeBold = true;
        for (ITSupportReplyDTO messageTmp : supportMessages) {
            tableMessage = new ITSupportTableRecord();
            tableMessage.setTableId(messageTmp.getReplyId());
            tableMessage.setTableTitle(messageTmp.getReplyTitle());
            tableMessage.setTableContent(messageTmp.getReplyContent());
            tableMessage.setSentTime(Time.getShortDate(messageTmp.getSentTime()));
            tableMessage.setSentUserId(messageTmp.getUserId());
            tableMessage.setOrigMessageId(messageTmp.getItSuppId());
            tableMessage.setStatus("");
            tableMessage.setStyle("");
            tableMessage.setStyle(tableMessage.getStyle() + "margin-left: 2em;");
            //result of a failed attempt to make objects bold. will be improved later.
            if (messageTmp.isITReply()) {
                makeBold = false;
            } else if ((isIT && makeBold) && !messageTmp.isITReply()) {
                tableMessage.setStyle(tableMessage.getStyle() + "font-weight:bold;");
            }
            if ((!isIT && !messageTmp.isUserRead())) {
                tableMessage.setStyle(tableMessage.getStyle() + "font-weight:bold;");
            }

            messages.add(tableMessage);
        }
        return messages;
    }

    /**
     * Runs of a specific object is selected by the IT. Used dynamically by
     * table object. Changes the message to display one of the selected object.
     * Will put in function to enable the "Reply" key which will prevent a null
     * pointer exception error
     *
     * @param event
     */
    public void onRowSelectIT(SelectEvent event) {
        selectedMessage = (ITSupportTableRecord) event.getObject();
        replyDisabledState = false;
        iTSupportMessage = selectedMessage.getTableTitle() + "\nSender : " + getFullnameFromId(selectedMessage.getSentUserId()) + "     Sent Time: " + selectedMessage.getSentTime() + "\n\n" + selectedMessage.getTableContent();
    }

    /**
     * Runs of a specific object is selected by the user. Used dynamically by
     * table object. Changes the message to display one of the selected object.
     * Will put in function to enable the "Reply" key which will prevent a null
     * pointer exception error
     *
     * @param event
     */
    public void onRowSelectUser(SelectEvent event) {
        selectedMessage = (ITSupportTableRecord) event.getObject();
        replyDisabledState = false;
        if (selectedMessage.getTableId() < 9000000) {
            new ITSupportController().isRead(selectedMessage.getTableId(), false, true);
        } else {
            new ITSupportReplyController().isRead(selectedMessage.getTableId() - 9000000);
        }
        iTSupportMessage = selectedMessage.getTableTitle() + "\nSender : " + getFullnameFromId(selectedMessage.getSentUserId()) + "     Sent Time: " + selectedMessage.getSentTime() + "\n\n" + selectedMessage.getTableContent();
    }

    /**
     * Runs when a specific object is unselected. Clears the current message.
     *
     * @param event
     */
    public void onRowUnselect(UnselectEvent event) {
        replyDisabledState = true;
        iTSupportMessage = new String();
    }

    /**
     * Function to get Full name from ID will be moved to accountDAO later.
     *
     * @param id
     * @return
     */
    public String getFullnameFromId(int id) {
        AccountDAO accountAccessObject = new AccountDAO();
        AccountDTO account;
        try {
            account = accountAccessObject.findFromId(id);
            return account.getFirstname() + " " + account.getLastname();
        } catch (SQLException | NamingException | ClassNotFoundException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return "";
    }

    /**
     * Edits status of currently selected items with selected status.
     */
    public void editStatus() {
        new ITSupportController().editStatus(currentSelectedStatus, selectedMessage.getOrigMessageId());
    }

    public boolean replyButtonState() {
        return replyDisabledState;
    }

    public ITSupportTableRecord getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(ITSupportTableRecord selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public String getiTSupportMessage() {
        return iTSupportMessage;
    }

    public void setiTSupportMessage(String iTSupportMessage) {
        this.iTSupportMessage = iTSupportMessage;
    }

    public boolean isShowActiveTickets() {
        return showActiveTickets;
    }

    public void setShowActiveTickets(boolean showActiveTickets) {
        this.showActiveTickets = showActiveTickets;
    }
    
    public void showActiveTickets(){
        showActiveTickets = true;
    }
    
    public void showCompletedTickets(){
        showActiveTickets = false;
    }

    public String getCurrentSelectedStatus() {
        return currentSelectedStatus;
    }

    public void setCurrentSelectedStatus(String currentSelectedStatus) {
        this.currentSelectedStatus = currentSelectedStatus;
    }
}
