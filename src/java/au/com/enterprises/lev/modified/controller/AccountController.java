package au.com.enterprises.lev.modified.controller;

import au.com.enterprises.lev.modified.model.AccountDTO;
import au.com.enterprises.lev.modified.Access.AccountDAO;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller for the account login database system.
 * @author Jonathan
 * @date 18/8/2016
 */
@Named
@RequestScoped
public class AccountController implements Serializable {

    private AccountDTO account = new AccountDTO();      //accoiunt object, vales set by calling HTML document.

    /**
     * Code for interfacing with the login system.(JDBCRealm) Gets username and
     * password, checks if login details entered matches database.
     *
     * @return
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(account.getUsername(), account.getPassword());
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
            return "login";
        }
        
        try {
            account = new AccountDAO().find(account.getUsername());
        } catch (ClassNotFoundException | SQLException | NamingException e)
        {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        
        //redirects users to correct page.
        switch (account.getGroupname()) {
            case "Users":
                return "allusers/welcome";
            case "Sysadmin":
                return "sysadmin/welcomeSys";
            case "Superusers":
                return "superusers/welcomeSu";
            default:
                return "allusers/welcome";
        }
    }

    /**
     * Logs user out of the system.
     * @return 
     */
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        //returns user to login page.
        return "/logout.xhtml";
    }

    /**
     * Retrieves the account object. Used by login.xhtml to communicate with
     * this class.
     *
     * @return
     */
    public AccountDTO getAccount() {
        return account;
    }

    /**
     * Retrieves all accounts in the database
     *
     * @return
     */
    public ArrayList<AccountDTO> getAccounts() {
        AccountDAO accessObject = new AccountDAO();
        return accessObject.findAll();
    }

    /**
     * Adds new account into the existing accounts database and then go
     * to "accountManagement" page.
     *
     * @return
     */
    public String saveAsNew() {
        AccountDAO accessObject = new AccountDAO();
        try {
            accessObject.add(account);
        } catch (SQLException | NamingException | NoSuchAlgorithmException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return "accountManagement";
    }

    /**
     * Retrieves account from username input
     *
     * @param username
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws NamingException
     */
    public void loadGroup(String username) throws ClassNotFoundException, SQLException, NamingException {
        AccountDAO accessObject = new AccountDAO();
        account = accessObject.find(username);
    }

    /**
     * Saves changes made to account
     *
     * @return
     */
    public String saveChanges() {
        AccountDAO accessObject = new AccountDAO();
        accessObject.update(account);
        return "accountManagement";
    }

    /**
     * Deletes account
     *
     * @return
     */
    public String delete() {
        AccountDAO accessObject = new AccountDAO();
        accessObject.delete(account);
        return "accountManagement";
    }
    
    public String getNameFromID(int userId) throws NamingException, SQLException{
        return new AccountDAO().getNameFromID(userId);
    }
}
