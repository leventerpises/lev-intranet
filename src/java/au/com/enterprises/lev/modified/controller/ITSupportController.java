package au.com.enterprises.lev.modified.controller;

import au.com.enterprises.lev.modified.model.*;
import au.com.enterprises.lev.modified.Access.*;
import au.com.enterprises.lev.modified.staticClasses.User;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import java.util.Date;

/**
 * Controller for ITSupport objects.
 *
 * @author Jonathan
 */
@Named
@RequestScoped
public class ITSupportController {

    private ArrayList<ITSupportDTO> allMessages;
    private ITSupportDTO itSupp;
    private ITSupportDAO mITSupportDAO;
    private ITSupportDTO selectedMessage;
    private String iTSupportMessage;
    
    
    public ITSupportController(){
        allMessages = new ArrayList<>();
        itSupp = new ITSupportDTO();
        mITSupportDAO = new ITSupportDAO();
        selectedMessage = new ITSupportDTO();
        iTSupportMessage = new String();
    }

    /**
     *
     * @return
     */
    public ArrayList<ITSupportDTO> getActiveMessages() {
        try {
            mITSupportDAO.startUp();
            allMessages = mITSupportDAO.findActive();
            mITSupportDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return allMessages;
    }
    
        public ArrayList<ITSupportDTO> getCompletedMessages() {
        try {
            mITSupportDAO.startUp();
            allMessages = mITSupportDAO.findCompleted();
            mITSupportDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return allMessages;
    }

    public ArrayList<ITSupportDTO> getUserActiveMessages() {
        try {
            mITSupportDAO.startUp();
            allMessages = mITSupportDAO.findActiveByUser(User.getUserId());
            mITSupportDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return allMessages;
    }
    
       public ArrayList<ITSupportDTO> getUserCompletedMessages() {
        try {
            mITSupportDAO.startUp();
            allMessages = mITSupportDAO.findCompletedByUser(User.getUserId());
            mITSupportDAO.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return allMessages;
    }

    public String addMessage() throws SQLException, NamingException {
        mITSupportDAO.startUp();
        try {
            itSupp.setSentTime(new Date());
            itSupp.setStatus("Started");
            itSupp.setUserId(User.getUserId());
            itSupp.setItRead(false);
            itSupp.setUserRead(true);

            mITSupportDAO.writeToTable(itSupp);
        } catch (SQLException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        } finally {
            mITSupportDAO.endConnection();
        }
        return "ticketList";
    }

    public String getFullnameFromId(int id) {
        AccountDAO accountAccessObject = new AccountDAO();
        AccountDTO account;
        try {
            account = accountAccessObject.findFromId(id);
            return account.getFirstname() + " " + account.getLastname();
        } catch (SQLException | NamingException | ClassNotFoundException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return "";
    }

    private ITSupportDTO findObjectFromId(int id) {
        ITSupportDAO ITAccessObject = new ITSupportDAO();
        ITSupportDTO ITSupport = new ITSupportDTO();
        try {
            ITAccessObject.startUp();
            ITSupport = ITAccessObject.findObjectFromID(id);
            ITAccessObject.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return (ITSupport);
    }

    public void editStatus(String status, int id) {
        ITSupportDTO ITSupport = findObjectFromId(id);
        ITSupport.setStatus(status);
        ITSupportDAO ITAccessObject = new ITSupportDAO();
        try {
            ITAccessObject.startUp();
            ITAccessObject.editRecord(ITSupport);
            ITAccessObject.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    public void isRead(int id, Boolean isIT, Boolean readState) {
        ITSupportDTO ITSupport = findObjectFromId(id);
        if (isIT)
        {
            ITSupport.setItRead(readState);
        }
        else
        {
            ITSupport.setUserRead(readState);
        }
        ITSupportDAO ITAccessObject = new ITSupportDAO();
        try {
            ITAccessObject.startUp();
            ITAccessObject.editRecord(ITSupport);
            ITAccessObject.endConnection();
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    public ITSupportDTO getItSupp() {
        return itSupp;
    }

    public void setItSupp(ITSupportDTO itSupp) {
        this.itSupp = itSupp;
    }

    public void setAllMessages(ArrayList<ITSupportDTO> allMessages) {
        this.allMessages = allMessages;
    }

    public ITSupportDAO getmITSupportDAO() {
        return mITSupportDAO;
    }

    public void setmITSupportDAO(ITSupportDAO mITSupportDAO) {
        this.mITSupportDAO = mITSupportDAO;
    }

    public ITSupportDTO getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(ITSupportDTO selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public String getiTSupportMessage() {
        return iTSupportMessage;
    }

    public void setiTSupportMessage(String iTSupportMessage) {
        this.iTSupportMessage = iTSupportMessage;
    }
}
