/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.model;

import java.sql.Timestamp;
import java.util.Date;

/**
 * DTO for the ITSupport.
 * @author Jonathan
 */
public class ITSupportDTO {
    private int itSuppId;           //ID of the support request, Set automatically by SQL
    private String requestTitle;    //Title of the IT Support request.
    private String requestContent;  //Content of the support request.
    private Date sentTime;          //Sent time of the support rquest.
    private String status;          //Status of support request. Can be Started, Pending or Completed.
    private int userId;             //ID of the user who sent request.
    private boolean itRead;         //value to store whether message is read by IT
    private boolean userRead;       //value to store whether message is read by user.

    public int getItSuppId() {
        return itSuppId;
    }

    public void setItSuppId(int itSuppId) {
        this.itSuppId = itSuppId;
    }

    public String getRequestTitle() {
        return requestTitle;
    }

    public void setRequestTitle(String requestTitle) {
        this.requestTitle = requestTitle;
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public Date getSentTime() {
        return sentTime;
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = sentTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isItRead() {
        return itRead;
    }

    public void setItRead(boolean itRead) {
        this.itRead = itRead;
    }

    public boolean isUserRead() {
        return userRead;
    }

    public void setUserRead(boolean userRead) {
        this.userRead = userRead;
    }
    
    
}
