/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.model;

import java.util.Date;

/**
 * DTO For ITReply object.
 * @author Jonathan
 */
public class ITSupportReplyDTO {
    private int replyId;            //IF of reply, set automatically by SQL.
    private String replyTitle;      //Title of IT reply.
    private String replyContent;    //Content of IT reply.
    private Date sentTime;          //Time the IT Reply is sent.
    private int userId;             //ID of the user who sent request.
    private int itSuppId;           //ID of the IT support who replied.
    private boolean ITReply;         //Value to store whether message is read by IT
    private boolean userRead;       //value to store whether message is read by user

    public int getReplyId() {
        return replyId;
    }

    public String getReplyTitle() {
        return replyTitle;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public Date getSentTime() {
        return sentTime;
    }

    public int getUserId() {
        return userId;
    }

    public int getItSuppId() {
        return itSuppId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public void setReplyTitle(String replyTitle) {
        this.replyTitle = replyTitle;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = sentTime;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setItSuppId(int itSuppId) {
        this.itSuppId = itSuppId;
    }

    public boolean isITReply() {
        return ITReply;
    }

    public void setITReply(boolean isITReply) {
        this.ITReply = isITReply;
    }

    public boolean isUserRead() {
        return userRead;
    }

    public void setUserRead(boolean userRead) {
        this.userRead = userRead;
    }
    
    
}
