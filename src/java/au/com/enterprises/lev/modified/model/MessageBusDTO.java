/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.model;

/**
 *
 * @author user
 */
public class MessageBusDTO{
    private int messageId;                  //id of message
    private String messageTitle;            //title of message
    private String messageContent;          //content of message
    private String sentTime;                //the time a message is sent
    private String readTime;                //the time a message is read
    private int receiverId;                 //id of the person who receives the message
    private int senderId;                   //id of the person who sends the message
    private boolean read;                   //true if the message has been read

    public int getMessageId() {
        return messageId;
    }

    public String getSentTime() {
        return sentTime;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public String getReadTime() {
        return readTime;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public void setReadTime(String readTime) {
        this.readTime = readTime;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
