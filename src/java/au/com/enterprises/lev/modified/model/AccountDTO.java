/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.model;

/**
 * DTO Object for account.
 * @author user
 */
public class AccountDTO {
    private int userId;             //ID for user. Value is set by SQL
    private String username;        //username of the account.
    private String password;        //password of account.
    private String firstname;       //Firstname of account holder.
    private String lastname;        //Lastname of accoutn holder.
    private String groupname;       //Group the account holder belongs to.
    private String email;           //Email of account holder. Possibly used in future.

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGroupname() {
        return groupname;
    }

    public String getEmail() {
        return email;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
