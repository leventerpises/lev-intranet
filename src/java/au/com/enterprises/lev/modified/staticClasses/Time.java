/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package au.com.enterprises.lev.modified.staticClasses;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**
 * Contains time functions.
 * @author Jonathan
 */
public class Time
{
    /**
     * Returns a string with a correctly formatted date.
     * @param date - date to be formatted.
     * @return 
     */
    public static String getShortDate(Date date)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        //get current date time with Date()
        return(dateFormat.format(date));
    }
    
}
