/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.staticClasses;

import au.com.enterprises.lev.modified.model.AccountDTO;
import javax.faces.context.FacesContext;
import au.com.enterprises.lev.modified.Access.AccountDAO;
import java.io.Serializable;
import javax.naming.NamingException;
import java.sql.SQLException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

/**
 * Contains basic user functions.
 * @author Jonathan
 */
@Named
@SessionScoped
public class User implements Serializable {

    /**
     * Gets username of current user.
     * @return 
     */
    public static String getUsername() {
        String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        return username;
    }

    /**
     * Gets the role of the current user.
     * @return 
     */
    public static String getRole() {
        String[] roles = {"Users", "Superusers", "Sysadmin"};
        for (String role : roles) {
            if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole(role))
            {
                return(role);
            }
        }
        return null;
    }

    /**
     * Gets ID of the current user.
     * @return 
     */
    public static int getUserId() {
        AccountDAO accountAccessObject = new AccountDAO();
        AccountDTO accountTmp = new AccountDTO();
        try {
            String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
            accountTmp = accountAccessObject.find(username);
        } catch (ClassNotFoundException | SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return accountTmp.getUserId();
    }
}
