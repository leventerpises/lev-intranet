/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.staticClasses;

import java.util.Random;

/**
 * Generates a random number, not currently used, will be useful later.
 * @author Jonathan
 */
public class RandomNumber {

    public static int generate() {
        return (new Random().nextInt(999999999));
    }
}
