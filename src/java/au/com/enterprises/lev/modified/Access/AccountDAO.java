/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.Access;

/**
 *
 * @author user
 */
/**
 * This class is the data access object of the account login system. This class
 * will retrieve vales and place values to and from the SQL database. Mostly
 * used by the AccountController class.
 */
import au.com.enterprises.lev.modified.model.AccountDTO;
import au.com.enterprises.lev.modified.staticClasses.Sha;

import java.sql.*;
import java.util.ArrayList;
import javax.naming.*;
import javax.sql.*;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * @author Jonathan
 * @Date 19/8/2016
 */
public class AccountDAO {

    /**
     * Login information for the SQL database.
     */
    private final String loginUsername = "LevAdmin";
    private final String loginPassword = "Cupcake1900";

    /**
     * @param username - username belonging to the object 
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws NamingException
     *
     * Finds an entry from the database, from a given username, and returns an
     * AccountDTO object. Used to retrieve one single entry from the SQL
     * database.
     */
    public AccountDTO find(String username) throws ClassNotFoundException, SQLException, NamingException {
        AccountDTO account = new AccountDTO();

        //Lookup and login to the SQL database.
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
        //Retreive values.
        try (Connection connect = ds.getConnection(loginUsername, loginPassword);
                PreparedStatement pstmt = connect.prepareStatement("select userId, username, password, email, firstname, lastname, groupname from levAccounts");
                ResultSet rs = pstmt.executeQuery()) {
            //set all values a ITSupportDTO object.
            while (rs.next()) {
                if (username.equals(rs.getString("username"))) {
                    account.setUserId(rs.getInt("userId"));
                    account.setUsername(rs.getString("username"));
                    account.setPassword(rs.getString("password"));
                    account.setFirstname(rs.getString("firstname"));
                    account.setLastname(rs.getString("lastname"));
                    account.setEmail(rs.getString("email"));
                    account.setGroupname(rs.getString("groupname"));
                }
            }
            rs.close();
            pstmt.close();
            connect.close();
        }
        //Return account object.
        return account;
    }

    /**
     * THis class finds all values in the accounts database and then returns all
     * the values in an ArrayList.
     *
     * @return
     */
    public ArrayList<AccountDTO> findAll() {
        ArrayList<AccountDTO> accountList = new ArrayList<>();

        //Lookup and login to the SQL database.
        try {
            DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
            //Retreive values.
            try (Connection connect = ds.getConnection(loginUsername, loginPassword); //Retreive values.
                    PreparedStatement pstmt = connect.prepareStatement("select userId, username, password, email, firstname, lastname, groupname from levAccounts"); 
                    ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    AccountDTO account = new AccountDTO();
                    account.setUserId(rs.getInt("userId"));
                    account.setUsername(rs.getString("username"));
                    account.setPassword(rs.getString("password"));
                    account.setEmail(rs.getString("email"));
                    account.setFirstname(rs.getString("firstname"));
                    account.setLastname(rs.getString("lastname"));
                    account.setGroupname(rs.getString("groupname"));
                    accountList.add(account);
                }
            }
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return accountList;
    }
    
    /**
     * returns full name of the user from the user id 
     * @param userId
     * @return
     * @throws NamingException
     * @throws SQLException 
     */
    public String getNameFromID(int userId) throws NamingException, SQLException{
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
        try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
            String sql = "SELECT userID, firstname, lastname FROM levAccounts WHERE userID = ?";
            PreparedStatement stmt = connect.prepareStatement(sql);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next())
                return rs.getString(2) + " " + rs.getString(3);
            return "";
        }
    }
    
    /**
     * returns a linked hash map that contains a map of user id and their names
     * function is used in user view
     * 
     * @return
     * @throws NamingException
     * @throws SQLException 
     */
    public Map getAllNames() throws NamingException, SQLException{
        Map<Integer, String> names = new LinkedHashMap<>();
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
        try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
            String sql = "SELECT userid, firstname, lastname FROM levAccounts";
            PreparedStatement stmt = connect.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                do{
                    names.put(rs.getInt(1),rs.getString(2)+rs.getString(3));
                } while (rs.next());
            };
        }
        return names;
    }

    /**
     * @param account - accountDTO object to be added to database.
     * @throws SQLException
     * @throws NamingException
     * @throws NoSuchAlgorithmException
     *
     * Adds new account into accounts database
     */
    public void add(AccountDTO account) throws SQLException, NamingException, NoSuchAlgorithmException {
        //Connects to database
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
        try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
            //Inserts values into database
            String sql = "INSERT INTO levAccounts(username, password, email, firstname, lastname, groupname) VALUES(?,?,?,?,?,?)";
            try (PreparedStatement ps = connect.prepareStatement(sql)) {
                ps.setString(1, account.getUsername());
                ps.setString(2, Sha.hash256(account.getPassword()));
                ps.setString(3, account.getEmail());
                ps.setString(4, account.getFirstname());
                ps.setString(5, account.getLastname());
                ps.setString(6, account.getGroupname());
                ps.executeUpdate();
            }
        }
    }

    /**
     * @param account - the accountDTO object to be edited.
     *
     * Updates existing account information from database. 
     * 
     */
    public void update(AccountDTO account) {
        try {
            //Connects to database
            DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
            //Sets variables to update and execute query to update database
            try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
                //Sets variables to update and execute query to update database
                String sql = "UPDATE levAccounts SET username = ?, password = ?, email = ?, firstname = ?, lastname = ?, groupname = ? WHERE userId = ?";
                try (PreparedStatement ps = connect.prepareStatement(sql)) {
                    ps.setString(1, account.getUsername());
                    ps.setString(2, account.getPassword());
                    ps.setString(3, account.getEmail());
                    ps.setString(4, account.getFirstname());
                    ps.setString(5, account.getLastname());
                    ps.setString(6, account.getGroupname());
                    ps.setInt(7, account.getUserId());
                    ps.executeUpdate();
                }
            }
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    /**
     *
     * @param account - AccountDTO object to be deleted.
     *
     * Deletes existing account from database
     *
     */
    public void delete(AccountDTO account) {
        try {
            //Connects to database
            DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
            //Query to be run from database
            try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
                //Query to be run from database
                String sql = "DELETE FROM levAccounts WHERE userId = ?";
                try ( //Sets key to lookup and delete
                        PreparedStatement ps = connect.prepareStatement(sql)) {
                    ps.setInt(1, account.getUserId());
                    //Execute query and delete data
                    ps.executeUpdate();
                    //Close all existing connections with the database
                }
            }
        } catch (SQLException | NamingException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    /**
     * 
     * @param id - ID used to find the account object from.
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws NamingException 
     * 
     * Finds account object from ID.
     * 
     */
    public AccountDTO findFromId(int id) throws ClassNotFoundException, SQLException, NamingException {
        AccountDTO account = new AccountDTO();

        //Lookup and login to the SQL database.
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
        String sql = "SELECT userId, username, password, email, firstname, lastname, groupname FROM levAccounts WHERE userId = ?";
        ResultSet rs;
        try (Connection connect = ds.getConnection(loginUsername, loginPassword)) {
            PreparedStatement pstmt;
            pstmt = connect.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                account.setUserId(rs.getInt("userId"));
                account.setUsername(rs.getString("username"));
                account.setPassword(rs.getString("password"));
                account.setFirstname(rs.getString("firstname"));
                account.setLastname(rs.getString("lastname"));
                account.setEmail(rs.getString("email"));
                account.setGroupname(rs.getString("groupname"));
            }
            rs.close();
            pstmt.close();
        }

        return account;
    }
}
