/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.Access;

import au.com.enterprises.lev.modified.model.MessageBusDTO;
import au.com.enterprises.lev.modified.staticClasses.Time;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Data Access Object for Message Bus, manipulates message bus database.
 * @author Anson
 */
public class MessageBusDAO {

    //Used for Connection
    private Connection mConnection; 
    //Used for querying
    private PreparedStatement mPreparedStatement;
    private final String username = "LevAdmin";
    private final String password = "Cupcake1990";

    /**
     * A function to start up connection to database
     */
    public void startUp() {
        if (mConnection == null || mPreparedStatement == null) {
            try {
                DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
                mConnection = ds.getConnection(username, password);
            } catch (NamingException | SQLException e) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(e.getMessage()));
            }
        }
    }

    /**
     * Adds new message into database
     * @param message
     * @param time
     * @throws SQLException 
     */ 
    public void add(MessageBusDTO message, Timestamp time) throws SQLException {
        String query = "INSERT INTO MessageBus "
                + "(messageTitle, message, sentTime, receiverID, senderId, seen)"
                + " Values (?, ?, ?, ?, ?, ?)";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setString(1, message.getMessageTitle());
        mPreparedStatement.setString(2, message.getMessageContent());
        mPreparedStatement.setTimestamp(3, time);
        mPreparedStatement.setInt(4, message.getReceiverId());
        mPreparedStatement.setInt(5, message.getSenderId());
        mPreparedStatement.setBoolean(6, false);
        executeUpdate();
    }

    /**
     * Updates database field "seen" and "readtime"
     * Accessed when a user clicks onto a message
     * @param message
     * @param time
     * @throws SQLException 
     */
    public void update(MessageBusDTO message, Timestamp time) throws SQLException {
        String query = "UPDATE MessageBus "
                + "SET readTime = ?, seen = ? "
                + "WHERE messageBusID = ?";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setTimestamp(1, time);
        mPreparedStatement.setBoolean(2, true);
        mPreparedStatement.setInt(3, message.getMessageId());
        executeUpdate();
    }

    /**
     * Executes prepared statement
     * @throws SQLException 
     */
    public void executeUpdate() throws SQLException {
        mPreparedStatement.executeUpdate();
        mConnection.commit();
    }

    /**
     * returns a list of messages according to the receiver id
     * @param userID
     * @return
     * @throws SQLException 
     */
    public ArrayList<MessageBusDTO> findByReceiver(int userID) throws SQLException {
        String query = "SELECT * FROM MessageBus"
                + " WHERE RECEIVERID = ? ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, userID);
        return getArrayListFromQuery();
    }

    /**
     * returns a list of messages according to the sender id
     * @param userID
     * @return
     * @throws SQLException 
     */
    public ArrayList<MessageBusDTO> findBySender(int userID) throws SQLException {
        String query = "SELECT * FROM MessageBus "
                + "WHERE SENDERID = ? ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, userID);
        return getArrayListFromQuery();
    }

    /**
     * used in conjunction with other functions
     * executes prepared statement and returns an array list of messages
     * @return
     * @throws SQLException 
     */
    private ArrayList<MessageBusDTO> getArrayListFromQuery() throws SQLException {
        ArrayList<MessageBusDTO> messages = new ArrayList<>();
        ResultSet rs = mPreparedStatement.executeQuery();
        while (rs.next()) {
            MessageBusDTO message = new MessageBusDTO();
            message.setMessageId(rs.getInt(1));
            message.setMessageTitle(rs.getString(2));
            message.setMessageContent(rs.getString(3));
            message.setSentTime(Time.getShortDate(rs.getTimestamp(4)));
            if (rs.getTimestamp(5) != null) {
                message.setReadTime(Time.getShortDate(rs.getTimestamp(5)));
            } else {
                message.setReadTime("Unread");
            }
            message.setReceiverId(rs.getInt(6));
            message.setSenderId(rs.getInt(7));
            message.setRead(rs.getBoolean(8));
            messages.add(message);
        }
        return messages;
    }

    /**
     * check if connection exists
     * @return 
     */
    public boolean isConnected() {
        return mConnection != null;
    }

    /**
     * removes all existing connection to the message database
     */
    public void endConnection() {
        if (mConnection != null) {
            try {
                mPreparedStatement.close();
                mConnection.close();
                mPreparedStatement = null;
                mConnection = null;
            } catch (SQLException ex) {
                Logger.getLogger(MessageBusDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
