/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.modified.Access;

import au.com.enterprises.lev.modified.model.ITSupportDTO;
;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.ejb.Startup;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Data Access Object for ITSupport database. Used to access ITSupport database.
 *
 * @author Jonathan Ho
 */


@Startup
public class ITSupportDAO {

    private Connection mConnection;                     //Used to store connection.
    private PreparedStatement mPreparedStatement;       //PreparedStatement, used to store PrepareStatement .
    private final String username;                      //username variable used to store username of database.
    private final String password;                      //Password variable, used to store password to access database.

    /**
     * Constructor for ITSupportDAO class. Sets username, password.
     */
    public ITSupportDAO() {
        mConnection = null;
        mPreparedStatement = null;
        username = "LevAdmin";
        password = "Cupcake1990";
    }

    /**
     * Startup command for ITSupportDAO. Used to initialize and access database.
     *
     * @throws SQLException
     * @throws NamingException
     */
    public void startUp() throws SQLException, NamingException {
        try {
            DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
            mConnection = ds.getConnection(username, password);
        } catch (NamingException | SQLException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
    }

    /**
     * Finds a record from the database from the ID and returns
     *
     * @param Id - Id of the object to be found.
     * @return
     * @throws SQLException
     */
    public ITSupportDTO findObjectFromID(int Id) throws SQLException {
        String query = "SELECT * FROM ItSupport "
                + "WHERE itSuppId = ?";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, Id);
        ITSupportDTO ITSupport = new ITSupportDTO();
        ResultSet rs = mPreparedStatement.executeQuery();
        //populate the ITSupportDTO object.
        while (rs.next()) {
            ITSupport.setItSuppId(rs.getInt(1));
            ITSupport.setRequestTitle(rs.getString(2));
            ITSupport.setRequestContent(rs.getString(3));
            ITSupport.setSentTime(rs.getTimestamp(4));
            ITSupport.setStatus(rs.getString(5));
            ITSupport.setUserId(rs.getInt(6));
            ITSupport.setItRead(rs.getBoolean(7));
            ITSupport.setUserRead(rs.getBoolean(8));
            return ITSupport;
        }
        return null;
    }

    /**
     * Gets a Result Set and places the RS into a ArrayList object.
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<ITSupportDTO> getArrayListFromQuery() throws SQLException {
        ArrayList<ITSupportDTO> ITSupports = new ArrayList<>();
        ResultSet rs = mPreparedStatement.executeQuery();
        ITSupportDTO ITSupport;
        while (rs.next()) {
            ITSupport = new ITSupportDTO();
            ITSupport.setItSuppId(rs.getInt(1));
            ITSupport.setRequestTitle(rs.getString(2));
            ITSupport.setRequestContent(rs.getString(3));
            ITSupport.setSentTime(rs.getTimestamp(4));
            ITSupport.setStatus(rs.getString(5));
            ITSupport.setUserId(rs.getInt(6));
            ITSupport.setItRead(rs.getBoolean(7));
            ITSupport.setUserRead(rs.getBoolean(8));
            ITSupports.add(ITSupport);
        }
        return ITSupports;
    }

    /**
     * Searches the database for all active records containing a particular userID.
     *
     * @param userID - ID of the user to be found.
     * @return
     * @throws SQLException
     */
    public ArrayList<ITSupportDTO> findActiveByUser(int userID) throws SQLException {
        String query = "SELECT * FROM ItSupport "
                + "WHERE userID = ? AND Status != 'Completed' ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, userID);
        return getArrayListFromQuery();
    }

    /**
     *  Searches the database for all completed records containing a particular userID.
     * @param userID - ID of the user to be found.
     * @return
     * @throws SQLException 
     */
    public ArrayList<ITSupportDTO> findCompletedByUser(int userID) throws SQLException {
        String query = "SELECT * FROM ItSupport "
                + "WHERE userID = ? AND Status = 'Completed' ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, userID);
        return getArrayListFromQuery();
    }

    /**
     * Searches the database and returns all active records.
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<ITSupportDTO> findActive() throws SQLException {
        String query = "SELECT * FROM ItSupport WHERE STATUS != 'Completed' ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        return getArrayListFromQuery();
    }

    /**
     * Searches the database and returns all completed records.
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<ITSupportDTO> findCompleted() throws SQLException {
        String query = "SELECT * FROM ItSupport WHERE STATUS = 'Completed' ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        return getArrayListFromQuery();
    }

    /**
     * Writes ITSupport Object into the database.
     *
     * @param itSupp - ITSupport object to be added to database.
     * @return
     * @throws SQLException
     */
    public boolean writeToTable(ITSupportDTO itSupp) throws SQLException {
        if (mConnection != null) {
            String query = "INSERT INTO ItSupport "
                    + "(requestTitle, requestContent, sentTime, status, userID, itRead, userRead) VALUES "
                    + "(?,?,?,?,?,?,?)";

            //set prepared statement.
            mPreparedStatement = mConnection.prepareStatement(query);
            mPreparedStatement.setString(1, itSupp.getRequestTitle());
            mPreparedStatement.setString(2, itSupp.getRequestContent());
            mPreparedStatement.setTimestamp(3, new java.sql.Timestamp(itSupp.getSentTime().getTime()));
            mPreparedStatement.setString(4, itSupp.getStatus());
            mPreparedStatement.setInt(5, itSupp.getUserId());
            mPreparedStatement.setBoolean(6, itSupp.isItRead());
            mPreparedStatement.setBoolean(7, itSupp.isUserRead());
            mPreparedStatement.executeUpdate();
            mConnection.commit();
            return true;
        }
        return false;
    }

    /**
     * Edit record of ITSupport table.
     *
     * @param itSupp -- ITSupport object to be edited in table.
     * @return
     * @throws SQLException
     */
    public boolean editRecord(ITSupportDTO itSupp) throws SQLException {
        if (mConnection != null) {
            //Update object where ID is the same as the input object.
            String query = "UPDATE ItSupport SET requestTitle = ?, requestContent = ?, sentTime = ?, status = ?, userID = ?, itRead = ?, userRead = ? WHERE ItSuppId = ?";
            mPreparedStatement = mConnection.prepareStatement(query);
            mPreparedStatement.setString(1, itSupp.getRequestTitle());
            mPreparedStatement.setString(2, itSupp.getRequestContent());
            mPreparedStatement.setTimestamp(3, new java.sql.Timestamp(itSupp.getSentTime().getTime()));
            mPreparedStatement.setString(4, itSupp.getStatus());
            mPreparedStatement.setInt(5, itSupp.getUserId());
            mPreparedStatement.setBoolean(6, itSupp.isItRead());
            mPreparedStatement.setBoolean(7, itSupp.isUserRead());
            mPreparedStatement.setInt(8, itSupp.getItSuppId());
            mPreparedStatement.executeUpdate();
            mConnection.commit();
            return true;
        }
        return false;
    }

    /**
     * End the connection of the database.
     *
     * @throws SQLException
     */
    public void endConnection() throws SQLException {
        mPreparedStatement.close();
        mConnection.close();
    }
}
