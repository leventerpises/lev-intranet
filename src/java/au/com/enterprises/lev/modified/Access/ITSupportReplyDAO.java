package au.com.enterprises.lev.modified.Access;

import au.com.enterprises.lev.modified.model.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Data access object for IT Reply.
 *
 * @author Jonathan Ho
 */
public class ITSupportReplyDAO {

    private final String loginUsername = "LevAdmin";
    private final String loginPassword = "Cupcake1900";
    private Connection mConnection;                     //Used to store connection.
    private PreparedStatement mPreparedStatement;       //PreparedStatement, used to store PrepareStatement .

    /**
     * starts connection to ITSupportReply database
     * @return
     * @throws SQLException
     * @throws NamingException 
     */
    public boolean startConnection() throws SQLException, NamingException {
        try {
            DataSource ds = (DataSource) InitialContext.doLookup("jdbc/levAccounts");
            mConnection = ds.getConnection(loginUsername, loginPassword);
        } catch (NamingException | SQLException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(e.getMessage()));
            return false;
        }
        return true;
    }

    /**
     * Ends connection
     * @throws SQLException 
     */
    public void endConnection() throws SQLException {
        mPreparedStatement.close();
        mConnection.close();
    }

    /**
     * Add SupportReply object to database.
     *
     * @param reply - SupportReply object to be added to database.
     * @throws SQLException
     * @throws NamingException
     * @throws NoSuchAlgorithmException
     */
    public void writeToTable(ITSupportReplyDTO reply) throws SQLException, NamingException, NoSuchAlgorithmException {
        String sql = "INSERT INTO ItSupportReplys(replyTitle, replyContent, sentTime, replyUserId, replyItSuppId, isItReply, userRead) VALUES(?,?,?,?,?,?,?)";
        mPreparedStatement = mConnection.prepareStatement(sql);
        mPreparedStatement.setString(1, reply.getReplyTitle());
        mPreparedStatement.setString(2, reply.getReplyContent());
        mPreparedStatement.setTimestamp(3, new java.sql.Timestamp(reply.getSentTime().getTime()));
        mPreparedStatement.setInt(4, reply.getUserId());
        mPreparedStatement.setInt(5, reply.getItSuppId());
        mPreparedStatement.setBoolean(6, reply.isITReply());
        mPreparedStatement.setBoolean(7, reply.isUserRead());
        mPreparedStatement.executeUpdate();
    }

    /**
     * Find all replies with the correct ITSuppID. Returns all replies for a
     * particular ITSupport item.
     *
     * @param ITSuppId - ID for ITSupport item to find replies for.
     * @return
     * @throws SQLException
     * @throws NamingException
     * @throws NoSuchAlgorithmException
     */
    public ArrayList<ITSupportReplyDTO> findReplies(int ITSuppId) throws SQLException, NamingException, NoSuchAlgorithmException {
        ITSupportReplyDTO reply;
        ArrayList<ITSupportReplyDTO> replyList = new ArrayList();
        String sql = "SELECT replyId, replyTitle, replyContent, sentTime, replyUserId, replyITSuppId, isITReply, userRead FROM ItSupportReplys WHERE replyItSuppId=? ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(sql);
        mPreparedStatement.setInt(1, ITSuppId);
        try (ResultSet rs = mPreparedStatement.executeQuery()) {
            //Puts all results into an arraylist.
            while (rs.next()) {
                reply = new ITSupportReplyDTO();
                reply.setReplyId(rs.getInt("replyId") + 9000000);
                reply.setReplyTitle(rs.getString("replyTitle"));
                reply.setReplyContent(rs.getString("replyContent"));
                reply.setSentTime(rs.getTimestamp("sentTime"));
                reply.setUserId(rs.getInt("replyUserId"));
                reply.setItSuppId(rs.getInt("replyItSuppId"));
                reply.setITReply(rs.getBoolean("isITReply"));
                reply.setUserRead(rs.getBoolean("userRead"));
                replyList.add(reply);
            }
        }
        //Returns ArrayList of found replies.
        return (replyList);
    }

    /**
     * Finds one reply based on reply ID.
     * @param replyID
     * @return
     * @throws SQLException
     * @throws NamingException 
     */
    public ITSupportReplyDTO findByReplyID(int replyID) throws SQLException, NamingException {
        ITSupportReplyDTO reply = new ITSupportReplyDTO();
        String query = "SELECT * FROM ItSupportReplys "
                + "WHERE replyID = ? ORDER BY sentTime DESC";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setInt(1, replyID);
        ResultSet rs = mPreparedStatement.executeQuery();
        while (rs.next()) {
            reply.setReplyId(rs.getInt("replyId") + 9000000);
            reply.setReplyTitle(rs.getString("replyTitle"));
            reply.setReplyContent(rs.getString("replyContent"));
            reply.setSentTime(rs.getTimestamp("sentTime"));
            reply.setUserId(rs.getInt("replyUserId"));
            reply.setItSuppId(rs.getInt("replyItSuppId"));
            reply.setITReply(rs.getBoolean("isITReply"));
            reply.setUserRead(rs.getBoolean("userRead"));
        }
        return reply;
    }

    /**
     * Changes values of a reply record with replyID.
     * @param itSupp - DTO containing values to change the new reply to.
     * @return
     * @throws SQLException
     * @throws NamingException 
     */
    public boolean editRecord(ITSupportReplyDTO itSupp) throws SQLException, NamingException {
        //Update object where ID is the same as the input object.
        String query = "UPDATE ItSupportReplys SET replyTitle = ?, replyContent = ?, sentTime = ?, replyUserId = ?, replyItSuppId = ?, isITReply = ?, userRead = ? WHERE replyId = ?";
        mPreparedStatement = mConnection.prepareStatement(query);
        mPreparedStatement.setString(1, itSupp.getReplyTitle());
        mPreparedStatement.setString(2, itSupp.getReplyContent());
        mPreparedStatement.setTimestamp(3, new java.sql.Timestamp(itSupp.getSentTime().getTime()));
        mPreparedStatement.setInt(4, itSupp.getUserId());
        mPreparedStatement.setInt(5, itSupp.getItSuppId());
        mPreparedStatement.setBoolean(6, itSupp.isITReply());
        mPreparedStatement.setBoolean(7, itSupp.isUserRead());
        mPreparedStatement.setInt(8, itSupp.getReplyId() - 9000000);
        mPreparedStatement.executeUpdate();
        mConnection.commit();
        return true;
    }
}
