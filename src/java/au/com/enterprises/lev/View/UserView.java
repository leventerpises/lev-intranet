/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.enterprises.lev.View;

import au.com.enterprises.lev.modified.Access.AccountDAO;
import au.com.enterprises.lev.modified.staticClasses.User;
import java.sql.SQLException;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.naming.NamingException;
/**
 * Function stores all view functions for the user. 
 * @author Jonathan
 */
@Named
@RequestScoped
public class UserView {
    //stores a list of users for sending messages
    private Map<Integer, String> userList;
    
    //returns different toolbars according to required page
    public String getToolbar()
    {
        switch (User.getRole()) {
            case "Users":
                return("/allusers/toolbar.xhtml");
            case "Superusers":
                return("/superusers/superusers_toolbar.xhtml");
            case "Sysadmin":
                return("/sysadmin/sysadmin_toolbar.xhtml");
            default:
                return null;
        }
    }
    
    public String getMenubar(){
        switch(User.getRole()){
            case "Users":
                return ("/allusers/menu.xhtml");
            case "Superusers":
                return("/superusers/menu.xhtml");
            case "Sysadmin":
                return ("/sysadmin/mennu.xhtml");
            default:
                return null;
        }
    }
    
    public Map getUserList() throws NamingException, SQLException{
        userList = new AccountDAO().getAllNames();
        return userList;
    }

    public void setUserList(Map<Integer, String> userList) {
        this.userList = userList;
    }
}
