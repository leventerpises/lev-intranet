DROP TABLE MessageBus;

CREATE TABLE MessageBus
(
    messagebusID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
    messageTitle VARCHAR(48) NOT NULL,
    message VARCHAR(255) NOT NULL,
    sentTime TIMESTAMP NOT NULL,
    readTime TIMESTAMP,
    receiverID INTEGER NOT NULL,
    senderId INTEGER NOT NULL,
    seen BOOLEAN NOT NULL,
    CONSTRAINT receiverID FOREIGN KEY (receiverID) 
    REFERENCES levAccounts(userID),
    CONSTRAINT senderID FOREIGN KEY (senderID) 
    REFERENCES levAccounts(userID)
);