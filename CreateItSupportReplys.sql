CREATE TABLE ItSupportReplys
(
    replyID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
    replyTitle VARCHAR(48) NOT NULL,
    replyContent VARCHAR(255) NOT NULL,
    sentTime TIMESTAMP NOT NULL,
    replyUserID INTEGER NOT NULL,
    replyItSuppId INTEGER NOT NULL,
    isItReply BOOLEAN NOT NULL,
    userRead BOOLEAN NOT NULL,
    CONSTRAINT replyUserID FOREIGN KEY (replyUserID) 
    REFERENCES levAccounts(userID),
    CONSTRAINT replyItSuppId FOREIGN KEY (replyItSuppId) 
    REFERENCES ItSupport(itSuppID)
)

