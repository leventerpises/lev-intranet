CREATE VIEW jdbcrealm_user (username, password) AS
SELECT username, password
from levAccounts;

create view jdbcrealm_group (username, groupname) as
select username, groupname
from levAccounts;